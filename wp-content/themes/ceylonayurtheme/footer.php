<?php wp_footer(); ?>
    <!-- Footer Start -->
    <footer class="section-t-space footer-section-2">
        <div class="container-fluid-lg">
            <div class="main-footer">
                <div class="row g-md-4 gy-sm-5 gy-2">
                    <div class="col-xxl-3 col-xl-4 col-sm-6">
                        <a href="index.html" class="foot-logo">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/logo/3.png" class="img-fluid" alt="">
                        </a>
                        <p class="information-text">Ceylon Ayuro (Pvt) Ltd is a Sri Lankan pharmaceutical company which focuses on the
                            research and development of pharmaceutical products and medicines with a special emphasis 
                            on health and wellness. Ceylon Ayuro products are manufactured using 100% natural in-grown ingredients. 
                            The herbs induced in our natural ingredients are being used for generations in the system of indigenous medicine
                            to promote health and wellness. Ceylon Ayoro is a company that has brought together age-old herbal remedy & modern 
                            technology to produce a range of herbal products using natural active ingredients.</p>                        
                    </div>

                    <div class="col-xxl-3 col-xl-4 col-sm-6">
                        <div class="footer-title">
                            <h4>Shop</h4>
                        </div>
                        <ul class="footer-list footer-contact mb-sm-0 mb-3">
                            <li>
                                <a href="about-us.html" class="footer-contain-2">
                                    <i class="fas fa-angle-right"></i>Diabsolve</a>
                            </li>
                            <li>
                                <a href="contact-us.html" class="footer-contain-2">
                                    <i class="fas fa-angle-right"></i>Skin Care Product</a>
                            </li>                            
                        </ul>
                    </div>

                    <div class="col-xxl-3 col-xl-4 col-sm-6">
                        <div class="footer-title">
                            <h4>Quick Links</h4>
                        </div>
                        <ul class="footer-list footer-contact mb-sm-0 mb-3">
                            <li>
                                <a href="order-success.html" class="footer-contain-2">
                                    <i class="fas fa-angle-right"></i>About Us</a>
                            </li>
                            <li>
                                <a href="user-dashboard.html" class="footer-contain-2">
                                    <i class="fas fa-angle-right"></i>Contact Us</a>
                            </li>
                            <li>
                                <a href="order-tracking.html" class="footer-contain-2">
                                    <i class="fas fa-angle-right"></i>Facts</a>
                            </li>                            
                        </ul>
                    </div>

                    

                    <div class="col-xxl-3 col-xl-4 col-sm-6">
                        <div class="footer-title">
                            <h4>Store infomation</h4>
                        </div>
                        <ul class="footer-address footer-contact">
                            <li>
                                <a href="javascript:void(0)">
                                    <div class="inform-box flex-start-box">
                                        <i data-feather="map-pin"></i>
                                        <p>Ceylon Ayuro (Pvt) Ltd.
26th Floor, East Tower, World Trade Center,
Echelon Square, Colombo 01,
Sri lanka.
</p>
                                    </div>
                                </a>
                            </li>

                            <li>
                                <a href="javascript:void(0)">
                                    <div class="inform-box">
                                        <i data-feather="phone"></i>
                                        <p>Call us: +94 77 200 8728</p>
                                    </div>
                                </a>
                            </li>

                            <li>
                                <a href="javascript:void(0)">
                                    <div class="inform-box">
                                        <i data-feather="mail"></i>
                                        <p>Email Us: ceylonayuro[at]gmail.com
</p>
                                    </div>
                                </a>
                            </li>

                            <li>
                                <a href="javascript:void(0)">
                                    <div class="inform-box">
                                        <i data-feather="printer"></i>
                                        <p>Fax: 123456</p>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        
                        <ul class="social-icon">
                            <li>
                                <a href="www.facebook.com">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a href="www.goolge.com">
                                    <i class="fab fa-google"></i>
                                </a>
                            </li>
                            <li>
                                <a href="www.twitter.com">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="www.instagram.com">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="www.pinterest.com">
                                    <i class="fab fa-pinterest-p"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="sub-footer section-small-space">
                <div class="left-footer">
                    <p>2023 Copyright By Ceylon Ayuro</p>
                </div>
                <div class="right-footer">
                    <ul class="payment-box">
                        <li>
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/icon/paymant/visa.png" alt="">
                        </li>
                        <li>
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/icon/paymant/discover.png" alt="">
                        </li>
                        <li>
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/icon/paymant/american.png" alt="">
                        </li>
                        <li>
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/icon/paymant/master-card.png" alt="">
                        </li>
                        <li>
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/icon/paymant/giro-pay.png" alt="">
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer End -->

    <!-- Quick View Modal Box Start -->
    <div class="modal fade theme-modal view-modal" id="view" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl modal-fullscreen-sm-down">
            <div class="modal-content">
                <div class="modal-header p-0">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fa-solid fa-xmark"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row g-sm-4 g-2">
                        <div class="col-lg-6">
                            <div class="slider-image">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/product/category/1.jpg" class="img-fluid blur-up lazyload"
                                    alt="">
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="right-sidebar-modal">
                                <h4 class="title-name">Peanut Butter Bite Premium Butter Cookies 600 g</h4>
                                <h4 class="price">$36.99</h4>
                                <div class="product-rating">
                                    <ul class="rating">
                                        <li>
                                            <i data-feather="star" class="fill"></i>
                                        </li>
                                        <li>
                                            <i data-feather="star" class="fill"></i>
                                        </li>
                                        <li>
                                            <i data-feather="star" class="fill"></i>
                                        </li>
                                        <li>
                                            <i data-feather="star" class="fill"></i>
                                        </li>
                                        <li>
                                            <i data-feather="star"></i>
                                        </li>
                                    </ul>
                                    <span class="ms-2">8 Reviews</span>
                                    <span class="ms-2 text-danger">6 sold in last 16 hours</span>
                                </div>

                                <div class="product-detail">
                                    <h4>Product Details :</h4>
                                    <p>Candy canes sugar plum tart cotton candy chupa chups sugar plum chocolate I love.
                                        Caramels marshmallow icing dessert candy canes I love soufflé I love toffee.
                                        Marshmallow pie sweet sweet roll sesame snaps tiramisu jelly bear claw. Bonbon
                                        muffin I love carrot cake sugar plum dessert bonbon.</p>
                                </div>

                                <ul class="brand-list">
                                    <li>
                                        <div class="brand-box">
                                            <h5>Brand Name:</h5>
                                            <h6>Black Forest</h6>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="brand-box">
                                            <h5>Product Code:</h5>
                                            <h6>W0690034</h6>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="brand-box">
                                            <h5>Product Type:</h5>
                                            <h6>White Cream Cake</h6>
                                        </div>
                                    </li>
                                </ul>

                                <div class="select-size">
                                    <h4>Cake Size :</h4>
                                    <select class="form-select select-form-size">
                                        <option selected>Select Size</option>
                                        <option value="1.2">1/2 KG</option>
                                        <option value="0">1 KG</option>
                                        <option value="1.5">1/5 KG</option>
                                        <option value="red">Red Roses</option>
                                        <option value="pink">With Pink Roses</option>
                                    </select>
                                </div>

                                <div class="modal-button">
                                    <button onclick="location.href = 'cart.html';"
                                        class="btn btn-md add-cart-button icon">Add
                                        To Cart</button>
                                    <button onclick="location.href = 'product-left.html';"
                                        class="btn theme-bg-color view-button icon text-white fw-bold btn-md">
                                        View More Details</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Quick View Modal Box End -->

    <!-- Location Modal Start -->
    <div class="modal location-modal fade theme-modal" id="locationModal" tabindex="-1"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-fullscreen-sm-down">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Choose your Delivery Location</h5>
                    <p class="mt-1 text-content">Enter your address and we will specify the offer for your area.</p>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fa-solid fa-xmark"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="location-list">
                        <div class="search-input">
                            <input type="search" class="form-control" placeholder="Search Your Area">
                            <i class="fa-solid fa-magnifying-glass"></i>
                        </div>

                        <div class="disabled-box">
                            <h6>Select a Location</h6>
                        </div>

                        <ul class="location-select custom-height">
                            <li>
                                <a href="javascript:void(0)">
                                    <h6>Alabama</h6>
                                    <span>Min: $130</span>
                                </a>
                            </li>

                            <li>
                                <a href="javascript:void(0)">
                                    <h6>Arizona</h6>
                                    <span>Min: $150</span>
                                </a>
                            </li>

                            <li>
                                <a href="javascript:void(0)">
                                    <h6>California</h6>
                                    <span>Min: $110</span>
                                </a>
                            </li>

                            <li>
                                <a href="javascript:void(0)">
                                    <h6>Colorado</h6>
                                    <span>Min: $140</span>
                                </a>
                            </li>

                            <li>
                                <a href="javascript:void(0)">
                                    <h6>Florida</h6>
                                    <span>Min: $160</span>
                                </a>
                            </li>

                            <li>
                                <a href="javascript:void(0)">
                                    <h6>Georgia</h6>
                                    <span>Min: $120</span>
                                </a>
                            </li>

                            <li>
                                <a href="javascript:void(0)">
                                    <h6>Kansas</h6>
                                    <span>Min: $170</span>
                                </a>
                            </li>

                            <li>
                                <a href="javascript:void(0)">
                                    <h6>Minnesota</h6>
                                    <span>Min: $120</span>
                                </a>
                            </li>

                            <li>
                                <a href="javascript:void(0)">
                                    <h6>New York</h6>
                                    <span>Min: $110</span>
                                </a>
                            </li>

                            <li>
                                <a href="javascript:void(0)">
                                    <h6>Washington</h6>
                                    <span>Min: $130</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Location Modal End -->

    <!-- Deal Box Modal Start -->
    <div class="modal fade theme-modal deal-modal" id="deal-box" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-fullscreen-sm-down">
            <div class="modal-content">
                <div class="modal-header">
                    <div>
                        <h5 class="modal-title w-100" id="deal_today">Deal Today</h5>
                        <p class="mt-1 text-content">Recommended deals for you.</p>
                    </div>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fa-solid fa-xmark"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="deal-offer-box">
                        <ul class="deal-offer-list">
                            <li class="list-1">
                                <div class="deal-offer-contain">
                                    <a href="shop-left-sidebar.html" class="deal-image">
                                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/vegetable/product/10.png" class="blur-up lazyload"
                                            alt="">
                                    </a>

                                    <a href="shop-left-sidebar.html" class="deal-contain">
                                        <h5>Blended Instant Coffee 50 g Buy 1 Get 1 Free</h5>
                                        <h6>$52.57 <del>57.62</del> <span>500 G</span></h6>
                                    </a>
                                </div>
                            </li>

                            <li class="list-2">
                                <div class="deal-offer-contain">
                                    <a href="shop-left-sidebar.html" class="deal-image">
                                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/vegetable/product/11.png" class="blur-up lazyload"
                                            alt="">
                                    </a>

                                    <a href="shop-left-sidebar.html" class="deal-contain">
                                        <h5>Blended Instant Coffee 50 g Buy 1 Get 1 Free</h5>
                                        <h6>$52.57 <del>57.62</del> <span>500 G</span></h6>
                                    </a>
                                </div>
                            </li>

                            <li class="list-3">
                                <div class="deal-offer-contain">
                                    <a href="shop-left-sidebar.html" class="deal-image">
                                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/vegetable/product/12.png" class="blur-up lazyload"
                                            alt="">
                                    </a>

                                    <a href="shop-left-sidebar.html" class="deal-contain">
                                        <h5>Blended Instant Coffee 50 g Buy 1 Get 1 Free</h5>
                                        <h6>$52.57 <del>57.62</del> <span>500 G</span></h6>
                                    </a>
                                </div>
                            </li>

                            <li class="list-1">
                                <div class="deal-offer-contain">
                                    <a href="shop-left-sidebar.html" class="deal-image">
                                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/vegetable/product/13.png" class="blur-up lazyload"
                                            alt="">
                                    </a>

                                    <a href="shop-left-sidebar.html" class="deal-contain">
                                        <h5>Blended Instant Coffee 50 g Buy 1 Get 1 Free</h5>
                                        <h6>$52.57 <del>57.62</del> <span>500 G</span></h6>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Deal Box Modal End -->

    <!-- Cookie Bar Box Start -->
    <div class="cookie-bar-box">
        <div class="cookie-box">
            <div class="cookie-image">
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/cookie-bar.png" class="blur-up lazyload" alt="">
                <h2>Cookies!</h2>
            </div>

            <div class="cookie-contain">
                <h5 class="text-content">We use cookies to make your experience better</h5>
            </div>
        </div>

        <div class="button-group">
            <a href="<?php echo get_permalink(3); ?>" target="_blank"><button class="btn privacy-button">Privacy Policy</button></a>
            <button class="btn ok-button">OK</button>
        </div>
    </div>
    <!-- Cookie Bar Box End -->

    <!-- Items section Start -->
    <div class="button-item">
        <button class="item-btn btn text-white">
            <i class="iconly-Bag-2 icli"></i>
        </button>
    </div>
    <div class="item-section">
        <button class="close-button">
            <i class="fas fa-times"></i>
        </button>
        <h6>
            <i class="iconly-Bag-2 icli"></i>
            <span>5 Items</span>
        </h6>
        <ul class="items-image">
            <li>
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/veg-3/cate1/1.png" alt="">
            </li>
            <li>
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/veg-3/cate1/2.png" alt="">
            </li>
            <li>+3</li>
        </ul>
        <button onclick="location.href = 'cart.html';" class="btn item-button btn-sm fw-bold">$ 20.70</button>
    </div>
    <!-- Items section End -->

    <!-- Tap to top start -->
    <div class="theme-option">
        <div class="back-to-top">
            <a id="back-to-top" href="#">
                <i class="fas fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <!-- Tap to top end -->

    <!-- Bg overlay Start -->
    <div class="bg-overlay"></div>
    <!-- Bg overlay End -->

    <!-- latest jquery-->
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/jquery-3.6.0.min.js"></script>

    <!-- jquery ui-->
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/jquery-ui.min.js"></script>

    <!-- Bootstrap js-->
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/bootstrap/bootstrap.bundle.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/bootstrap/popper.min.js"></script>

    <!-- feather icon js-->
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/feather/feather.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/feather/feather-icon.js"></script>

    <!-- Lazyload Js -->
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/lazysizes.min.js"></script>

    <!-- Slick js-->
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/slick/slick.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/bootstrap/bootstrap-notify.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/slick/custom_slick.js"></script>

    <!-- Auto Height Js -->
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/auto-height.js"></script>

    <!-- Quantity Js -->
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/quantity.js"></script>

    <!-- Timer Js -->
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/timer1.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/timer2.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/timer3.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/timer4.js"></script>

    <!-- Fly Cart Js -->
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/fly-cart.js"></script>

    <!-- WOW js -->
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/wow.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/custom-wow.js"></script>

    <!-- script js -->
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/script.js"></script>