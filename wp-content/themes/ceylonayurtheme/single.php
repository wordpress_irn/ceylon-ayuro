<?php
/**
 * Template Name:  Empty Template
 * Description: Empty Template
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage ceylonAyuro
 * @since ceylonAyuro 1.0
 */
get_header();
$page = get_post();

$page_title = ucwords($page->post_title);
$content = $page->post_content;
$content = apply_filters('the_content', $content);

//$sliders = get_field('sliders', $page->ID); 
?>

<!-- Breadcrumb Section Start -->
<section class="breadscrumb-section pt-0">
    <div class="container-fluid-lg">
        <div class="row">
            <div class="col-12">
                <div class="breadscrumb-contain">
                    <h2><?php echo $page_title; ?></h2>
                    <nav>
                        <ol class="breadcrumb mb-0">
                            <li class="breadcrumb-item">
                                <a href="index.html">
                                    <i class="fa-solid fa-house"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page"><?php echo $page_title; ?></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Section End -->


<!-- Client Section Start -->
<section class="client-section section-lg-space">
    <div class="container-fluid-lg">
        <div class="row">
            <div class="col-12">
                <?php echo $content; ?>
            </div>
        </div>
    </div>
</section>
<!-- Client Section End -->

<!-- Offer Section Start -->
<section class="offer-section">
    <div class="container-fluid-lg">
        <div class="row">
            <div class="col-12">
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/ad_banner_1.jpg" class="blur-up lazyload" alt="">                    
            </div>
        </div>
    </div>
</section>
<!-- Offer Section End -->

<?php get_footer(); ?>
</body>

</html>