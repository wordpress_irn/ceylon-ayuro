<?php
/**
 * Template Name:  Home
 * Description: Home
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage ceylonAyuro
 * @since ceylonAyuro 1.0
 */
get_header();
//$page = get_post($page->ID);

//$sliders = get_field('sliders', $page->ID); 
?>
<!-- Home Section Start -->
    <section class="home-section-2 home-section-bg pt-0 overflow-hidden">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-12">
                    <div class="slider-animate">
                        <div>
                            <div class="home-contain rounded-0 p-0">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/custom/diabsolve-slider.jpg"
                                    class="img-fluid bg-img blur-up lazyload" alt="">
                                <div class="home-detail home-big-space p-center-left home-overlay position-relative">
                                    <div class="container-fluid-lg">
                                        <div>
                                            <h6 class="ls-expanded theme-color text-uppercase">Weekend Special offer
                                            </h6>
                                            <h1 class="heding-2">DIABSOLVE</h1>
                                            <h2 class="content-2">Medical Supplement </h2>
                                            <h5 class="text-content">For Uncontrolled Diabetes
                                            </h5>
                                            <button
                                                class="btn theme-bg-color btn-md text-white fw-bold mt-md-4 mt-2 mend-auto"
                                                onclick="location.href = 'shop-left-sidebar.html';">Shop Now <i
                                                    class="fa-solid fa-arrow-right icon"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Home Section End -->
    
<!-- Fresh Vegetable Section Start -->
<section class="fresh-vegetable-section section-lg-space">
    <div class="container-fluid-lg">
        <div class="row gx-xl-5 gy-xl-0 g-3 ratio_148_1">
            <div class="col-xl-12 col-12">
                <div class="review-title">                    
                    <h2>Ceylon Ayuro</h2>
                </div>

                <p class="text-content">
                    Ceylon Ayuro products which is made using natural ingredients and herbs, heal your life with the goodness of Ayurveda. Ayurveda is a traditional form of medicine. With the formation of ancient human civilizations, human beings during that particular period of time had to fight against various diseases and plagues that were prevalent then. As a result, Ayurveda can be termed as a medical system that was initiated with the aim of improving the health conditions of human beings by focusing on all health spaces.
                </p>
            </div>
            <div class="col-xl-6 col-12">
                <div class="row g-sm-4 g-2">
                    <div class="col-6">
                        <div class="fresh-image-2">
                            <div>
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/custom/ceylon-ayuro-ingredients-1.jpg"
                                     class="bg-img blur-up lazyload" alt="">
                            </div>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="fresh-image">
                            <div>
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/custom/ceylon-ayuro-ingredients-2.jpg"
                                     class="bg-img blur-up lazyload" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-6 col-12">
                <div class="fresh-contain p-center-left">
                    <div>
                        <div class="delivery-list">
                            <p class="text-content">The primary objective of Ayuro is to be your most trusted health care partner while reinventing the 
                                values of Ayurveda through the intervention of modern technology; while preserving its natural goodness. Ceylon Ayuro 
                                is constantly working on and periodically reviews its products that are already in the market.</p>

                            <p class="text-content">Ayurveda is a holistic form of medicine manufactured from natural sources which have been enhancing a healthy way of 
                                life since ancient times.  It is founded on a concept of safety and efficacy in Ayurveda products, which explicitly 
                                avoid harmful side effects, providing the basis of quality standards.  The result has been traditional and authentically 
                                eco-friendly and consumer-friendly natural medicinal products</p>

                            <ul class="delivery-box">
                                <li>
                                    <div class="delivery-box">
                                        <div class="delivery-icon">
                                            <img src="<?php bloginfo('template_directory'); ?>/assets/svg/3/delivery.svg" class="blur-up lazyload" alt="">
                                        </div>

                                        <div class="delivery-detail">
                                            <h5 class="text">All shipping within Sri Lanka will be delivered within one day.</h5>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Fresh Vegetable Section End -->


<!-- Banner Section Start -->
    <section class="bank-section overflow-hidden">
        <div class="container-fluid-lg">
            <div class="title">
                <h2>Ceylon Ayuro</h2>
            </div>
            <div class="slider-bank-3 arrow-slider slick-height">
                <div>
                    <div class="bank-offer">
                        <div class="bank-header">
                            <div class="bank-left w-100">                                
                                <div class="bank-name">
                                    <p>We aim to make value added products from Sri Lanka’s natural resources particularly herbal plants, 
                                        ayurvedic medicinal remedies and crops with medicinal properties</p>
                                </div>
                            </div>

                            <div class="bank-right w-100">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/custom/3-1.jpg" class="img-fluid" alt="">
                            </div>
                        </div>

                        <div class="bank-footer bank-footer-1">
                            <h3>Focus on Natural Herbal</h3>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="bank-offer">
                        <div class="bank-header">
                            <div class="bank-left w-100">                                
                                <div class="bank-name">
                                    <p>We continue to commercialize plant based medicine and food supplement products from 
                                        Sri Lanka with worldwide export potential.</p>
                                </div>
                            </div>

                            <div class="bank-right w-100">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/custom/3-2.jpg" class="img-fluid" alt="">
                            </div>
                        </div>

                        <div class="bank-footer bank-footer-2">
                            <h3>Commercial Manufacturing</h3>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="bank-offer">
                        <div class="bank-header">
                            <div class="bank-left w-100">                                
                                <div class="bank-name">
                                    <p>We want to turn Sri Lanka into a forefront runner within research, production and export of 
                                        plant-based medicines and pharmaceutical products</p>
                                </div>
                            </div>

                            <div class="bank-right w-100">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/custom/3-3.jpg" class="img-fluid" alt="">
                            </div>
                        </div>

                        <div class="bank-footer bank-footer-3">
                            <h3>R & D on Plant Based Medicine</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Banner Section End -->
    
    <!-- Newsletter Section Start -->
    <section class="newsletter-section section-b-space">
        <div class="container-fluid-lg">
            <div class="newsletter-box newsletter-box-2">
                <div class="newsletter-contain py-5">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xxl-4 col-lg-5 col-md-7 col-sm-9 offset-xxl-2 offset-md-1">
                                <div class="newsletter-detail">
                                    <h2>Join our newsletter and get...</h2>
                                    <h5>Rs.500 discount for your first order</h5>
                                  <?php /*  <div class="input-box">
                                        <input type="email" class="form-control" id="exampleFormControlInput1"
                                            placeholder="Enter Your Email">
                                        <i class="fa-solid fa-envelope arrow"></i>
                                        <button class="sub-btn  btn-animation">
                                            <span class="d-sm-block d-none">Subscribe</span>
                                            <i class="fa-solid fa-arrow-right icon"></i>
                                        </button>
                                    </div> <?php */ ?>
                                    
                                    
                                    <?php echo do_shortcode('[contact-form-7 id="17" title="subscription"]'); ?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Newsletter Section End -->


<?php get_footer(); ?>
</body>

</html>