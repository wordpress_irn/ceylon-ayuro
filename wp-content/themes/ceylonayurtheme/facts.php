<?php
/**
 * Template Name:  Facts
 * Description: Facts
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage ceylonAyuro
 * @since ceylonAyuro 1.0
 */
get_header();
//$page = get_post($page->ID);

//$sliders = get_field('sliders', $page->ID); 
?>

<!-- Breadcrumb Section Start -->
<section class="breadscrumb-section pt-0">
    <div class="container-fluid-lg">
        <div class="row">
            <div class="col-12">
                <div class="breadscrumb-contain">
                    <h2>DIABETIC FACTS</h2>
                    <nav>
                        <ol class="breadcrumb mb-0">
                            <li class="breadcrumb-item">
                                <a href="index.html">
                                    <i class="fa-solid fa-house"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Diabetic Facts</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Section End -->

<!-- Contact Box Section Start -->
<section class="contact-box-section">
    <div class="container-fluid-lg">
        <div class="row g-lg-5 g-3">
            <div class="col-lg-6">
                <div class="left-sidebar-box">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="contact-image">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/diabetics.jpg"
                                     class="img-fluid blur-up lazyloaded" alt="">
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="title d-xxl-none d-block">
                    
                </div>
                <div class="right-sidebar-box">
                    <div class="row">
                        
                        <h4><strong>What is Diabetes?</strong></h4>
                        <br>
                        <p class="text-content">Diabetes is a chronic (long-lasting) health condition that affects how your body turns food into energy. 
                            Your body breaks down most of the food you eat into sugar (glucose) and releases it into your bloodstream. 
                            When your blood sugar goes up, it signals your pancreas to release insulin. Insulin acts like a key to 
                            let the blood sugar into your body’s cells for use as energy. With diabetes, your body doesn’t make enough 
                            insulin or can’t use it as well as it should. When there isn’t enough insulin or cells stop responding to insulin, 
                            too much blood sugar stays in your bloodstream. Over time, that can cause serious health problems, 
                            such as heart disease, vision loss, and kidney disease.</p>
                    </div>                        
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Contact Box Section End -->

<section>
    <div class="container">
        <div class="col-12">
            <div class="product-section-box">
                <ul class="nav nav-tabs custom-nav" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="description-tab" data-bs-toggle="tab"
                                data-bs-target="#tab_1" type="button" role="tab" aria-controls="tab_1"
                                aria-selected="true">Types of Diabetes?</button>
                    </li>

                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="info-tab" data-bs-toggle="tab" data-bs-target="#tab_2"
                                type="button" role="tab" aria-controls="tab_2" aria-selected="false">Diabetes Risk Factors?</button>
                    </li>

                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="care-tab" data-bs-toggle="tab" data-bs-target="#tab_3"
                                type="button" role="tab" aria-controls="tab_3" aria-selected="false">Diabetes Symptoms?</button>
                    </li>

                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="review-tab" data-bs-toggle="tab" data-bs-target="#tab_4"
                                type="button" role="tab" aria-controls="tab_4"
                                aria-selected="false">Diabetes Complications?</button>
                    </li>
                </ul>

                <div class="tab-content custom-tab" id="myTabContent">
                    <div class="tab-pane fade show active" id="tab_1" role="tabpanel"
                         aria-labelledby="description-tab">
                        <div class="product-description">
                            <div class="nav-desh">
                                <p>There are three main types of diabetes: type 1, type 2, and gestational diabetes (diabetes while pregnant).</p>
                            </div>

                            <div class="nav-desh">
                                <h5><strong>Type 1 Diabetes</strong></h5>
                                <p>Type 1 diabetes is thought to be caused by an autoimmune reaction (the body attacks itself by mistake). 
                                    This reaction stops your body from making insulin. Approximately 5-10% of the people who have diabetes have 
                                    type 1. Symptoms of type 1 diabetes often develop quickly. It’s usually diagnosed in children, teens, 
                                    and young adults. If you have type 1 diabetes, you’ll need to take insulin every day to survive. Currently, 
                                    no one knows how to prevent type 1 diabetes.</p>
                            </div>

                            <div class="nav-desh">
                                <h5><strong>Type 2 Diabetes</strong></h5>
                                <p>With type 2 diabetes, your body doesn’t use insulin well and can’t keep blood sugar at normal levels. 
                                    About 90-95% of people with diabetes have type 2. It develops over many years and is usually diagnosed 
                                    in adults (but more and more in children, teens, and young adults). You may not notice any symptoms, 
                                    so it’s important to get your blood sugar tested if you’re at risk. Type 2 diabetes can be prevented 
                                    or delayed with healthy lifestyle changes.</p>
                            </div>

                            <div class="nav-desh">
                                <h5><strong>Gestational Diabetes</strong></h5>
                                <p>Gestational diabetes develops in pregnant women who have never had diabetes. 
                                    If you have gestational diabetes, your baby could be at higher risk for health problems. 
                                    Gestational diabetes usually goes away after your baby is born. However, it increases your 
                                    risk for type 2 diabetes later in life. Your baby is more likely to have obesity as a child 
                                    or teen and develop type 2 diabetes later in life.</p>
                            </div>

                            <div class="nav-desh">
                                <h5><strong>Prediabetes</strong></h5>
                                <p>In the United States, 96 million adults—more than 1 in 3—have prediabetes. 
                                    More than 8 in 10 of them don’t know they have it. With prediabetes, blood 
                                    sugar levels are higher than normal, but not high enough for a type 2 diabetes 
                                    diagnosis. Prediabetes raises your risk for type 2 diabetes, heart disease, and stroke. 
                                    But there’s good news. If you have prediabetes, a CDC-recognized lifestyle change program 
                                    can help you take healthy steps to reverse it.</p>
                            </div>                            

                        </div>
                    </div>

                    <div class="tab-pane fade" id="tab_2" role="tabpanel" aria-labelledby="info-tab">
                        <div class="information-box">
                            <ul>
                                <li>Have prediabetes.</li>

                                <li>Are overweight.</li>

                                <li>Are 45 years or older.</li>

                                <li>Have a parent, brother, or sister with type 2 diabetes.</li>

                                <li>Are physically active less than 3 times a week.</li>

                                <li>Have ever had gestational diabetes (diabetes during pregnancy) or given birth to a baby who weighed over 9 pounds.</li>

                                <li>Are an African American, Hispanic or Latino, American Indian, or Alaska Native person. Some Pacific Islanders and Asian American people are also at higher risk.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="tab_3" role="tabpanel" aria-labelledby="care-tab">
                        <div class="information-box">
                            <ul>
                                <li>Urinate (pee) a lot, often at night</li>

                                <li>Are very thirsty</li>

                                <li>Lose weight without trying</li>

                                <li>Are very hungry</li>

                                <li>Have blurry vision</li>

                                <li>Have numb or tingling hands or feet</li>

                                <li>Feel very tired</li>

                                <li>Have very dry skin</li>

                                <li>Have sores that heal slowly</li>

                                <li>Have more infections than usual</li>
                            </ul>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="tab_4" role="tabpanel" aria-labelledby="review-tab">
                        <div class="review-box">
                            <div class="row g-4">
                                <div class="col-xl-6">
                                    <div class="review-title">
                                        <h4 class="fw-500">Diabetes Complications</h4>
                                    </div>

                                    <div class="information-box">
                                        <ul>
                                            <li>Heart and blood vessel (cardiovascular) disease. Diabetes majorly increases the risk of many heart problems</li>

                                            <li>Nerve damage (neuropathy)</li>

                                            <li>Kidney damage (nephropathy)</li>

                                            <li>Eye damage (retinopathy)</li>

                                            <li>Foot damage</li>

                                            <li>Skin and mouth conditions</li>

                                            <li>Hearing impairment</li>

                                            <li>Alzheimer's disease</li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</section>

<!-- Offer Section Start -->
<section class="offer-section">
    <div class="container-fluid-lg">
        <div class="row">
            <div class="col-12">
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/ad_banner_1.jpg" class="blur-up lazyload" alt="">                    
            </div>
        </div>
    </div>
</section>
<!-- Offer Section End -->

<?php get_footer(); ?>
</body>

</html>