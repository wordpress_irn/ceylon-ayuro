<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Fastkart">
        <meta name="keywords" content="Fastkart">
        <meta name="author" content="Fastkart">
        <link rel="icon" href="<?php bloginfo('template_directory'); ?>/assets/images/favicon/4.png" type="image/x-icon">
        <title>Ceylon Ayuro (pvt) Ltd</title>

        <!-- Google font -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Russo+One&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Exo+2:wght@400;500;600;700;800;900&display=swap"
              rel="stylesheet">
        <link
            href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
            rel="stylesheet">

        <!-- bootstrap css -->
        <link id="rtl-link" rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/vendors/bootstrap.css">

        <!-- wow css -->
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/animate.min.css" />

        <!-- font-awesome css -->
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/vendors/font-awesome.css">

        <!-- feather icon css -->
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/vendors/feather-icon.css">

        <!-- slick css -->
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/vendors/slick/slick.css">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/vendors/slick/slick-theme.css">

        <!-- Iconly css -->
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/bulk-style.css">

        <!-- Template css -->
        <link id="color-link" rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/style.css">
        <link id="color-link" rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/custom_styles.css">
    </head>

    <body class="theme-color-5 bg-effect">

        <!-- Loader Start -->
        <div class="fullpage-loader">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
        <!-- Loader End -->

        <!-- Header Start -->
        <header class="header-2">

            <div class="top-nav top-header sticky-header sticky-header-3">
                <div class="container-fluid-lg">
                    <div class="row">
                        <div class="col-12">
                            <div class="navbar-top">
                                <button class="navbar-toggler d-xl-none d-block p-0 me-3" type="button"
                                        data-bs-toggle="offcanvas" data-bs-target="#primaryMenu">
                                    <span class="navbar-toggler-icon">
                                        <i class="iconly-Category icli theme-color"></i>
                                    </span>
                                </button>
                                <a href="index.html" class="web-logo nav-logo">
                                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/logo/3.png" class="img-fluid blur-up lazyload" alt="">
                                </a>

                                <div class="search-full">
                                    <div class="input-group">
                                        <span class="input-group-text">
                                            <i data-feather="search" class="font-light"></i>
                                        </span>
                                        <input type="text" class="form-control search-type" placeholder="Search here..">
                                        <span class="input-group-text close-search">
                                            <i data-feather="x" class="font-light"></i>
                                        </span>
                                    </div>
                                </div>

                                <div class="middle-box">
                                    <div class="center-box">
                                        <div class="searchbar-box order-xl-1 d-none d-xl-block">
                                            <input type="search" class="form-control" id="exampleFormControlInput1"
                                                   placeholder="search for product, delivered to your door...">
                                            <button class="btn search-button">
                                                <i class="iconly-Search icli"></i>
                                            </button>
                                        </div>
                                        
                                    </div>
                                </div>

                                <div class="rightside-menu">
                                    <div class="dropdown-dollar">
                                        <div class="dropdown">
                                            <button class="dropdown-toggle" type="button" id="dropdownMenuButton1"
                                                    data-bs-toggle="dropdown" aria-expanded="false">
                                                <span>Language</span> <i class="fa-solid fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                <li>
                                                    <a id="eng" class="dropdown-item" href="javascript:void(0)">English</a>
                                                </li>
                                                <li>
                                                    <a id="hin" class="dropdown-item" href="javascript:void(0)">Sinhala</a>
                                                </li>
                                                <li>
                                                    <a id="guj" class="dropdown-item" href="javascript:void(0)">Tamil</a>
                                                </li>                                                
                                            </ul>
                                        </div>

                                        
                                    </div>

                                    <div class="option-list">
                                        <ul>
                                            <li>
                                                <a href="javascript:void(0)" class="header-icon user-icon search-icon">
                                                    <i class="iconly-Profile icli"></i>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="javascript:void(0)" class="header-icon search-box search-icon">
                                                    <i class="iconly-Search icli"></i>
                                                </a>
                                            </li>


                                            <li class="onhover-dropdown">
                                                <a href="cart.html" class="header-icon bag-icon">
                                                    <small class="badge-number">2</small>
                                                    <i class="iconly-Bag-2 icli"></i>
                                                </a>
                                                <div class="onhover-div">
                                                    <ul class="cart-list">
                                                        <li>
                                                            <div class="drop-cart">
                                                                <a href="product-left-thumbnail.html" class="drop-image">
                                                                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/vegetable/product/1.png"
                                                                         class="blur-up lazyload" alt="">
                                                                </a>

                                                                <div class="drop-contain">
                                                                    <a href="product-left-thumbnail.html">
                                                                        <h5>Fantasy Crunchy Choco Chip Cookies</h5>
                                                                    </a>
                                                                    <h6><span>1 x</span> $80.58</h6>
                                                                    <button class="close-button">
                                                                        <i class="fa-solid fa-xmark"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </li>

                                                        <li>
                                                            <div class="drop-cart">
                                                                <a href="product-left-thumbnail.html" class="drop-image">
                                                                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/vegetable/product/2.png"
                                                                         class="blur-up lazyload" alt="">
                                                                </a>

                                                                <div class="drop-contain">
                                                                    <a href="product-left-thumbnail.html">
                                                                        <h5>Peanut Butter Bite Premium Butter Cookies 600 g
                                                                        </h5>
                                                                    </a>
                                                                    <h6><span>1 x</span> $25.68</h6>
                                                                    <button class="close-button">
                                                                        <i class="fa-solid fa-xmark"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>


                                                    <div class="price-box">
                                                        <h5>Price :</h5>
                                                        <h4 class="theme-color fw-bold">$106.58</h4>
                                                    </div>

                                                    <div class="button-group">
                                                        <a href="cart.html" class="btn btn-sm cart-button">View Cart</a>
                                                        <a href="checkout.html" class="btn btn-sm cart-button theme-bg-color
                                                           text-white">Checkout</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-notification theme-bg-color overflow-hidden py-2">
                <div class="notification-slider">
                    <div>
                        <div class="timer-notification text-center">
                            <h6><strong class="me-1">Welcome to Ceylon Ayuro!</strong>
                            </h6>
                        </div>
                    </div>
                    <div>
                        <div class="timer-notification text-center">
                            <h6>All shipping within Sri Lanka will be delivered within one day.
                            </h6>
                        </div>
                    </div>
                </div>                
            </div>
            <div class="container-fluid-lg">
                <div class="row">
                    <div class="col-12">
                        <div class="main-nav">                            

                            <div class="main-nav navbar navbar-expand-xl navbar-light navbar-sticky">
                                <div class="offcanvas offcanvas-collapse order-xl-2" id="primaryMenu">
                                    <div class="offcanvas-header navbar-shadow">
                                        <h5>Menu</h5>
                                        <button class="btn-close lead" type="button" data-bs-dismiss="offcanvas"
                                                aria-label="Close"></button>
                                    </div>
                                    <div class="offcanvas-body">
                                        <ul class="navbar-nav">
                                            <li class="nav-item">
                                                <a class="nav-link ps-xl-2 ps-0" href="<?php echo get_permalink(5); ?>">Home</a>                                            
                                            </li>

                                            <li class="nav-item">
                                                <a class="nav-link ps-xl-2 ps-0" href="#">Shop</a>                                            
                                            </li>
                                            
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="javascript:void(0)"
                                                    data-bs-toggle="dropdown">About Us</a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a class="dropdown-item" href="<?php echo get_permalink(10); ?>">About Us</a>
                                                    </li>
                                                    <li>
                                                        <a class="dropdown-item" href="<?php echo get_permalink(10); ?>">Vision</a>
                                                    </li>
                                                    <li>
                                                        <a class="dropdown-item" href="<?php echo get_permalink(10); ?>">Mission</a>
                                                    </li>
                                                    <li>
                                                        <a class="dropdown-item" href="<?php echo get_permalink(10); ?>">Values</a>
                                                    </li>                                                    
                                                    <li>
                                                        <a class="dropdown-item" href="<?php echo get_permalink(8); ?>">Contact Us</a>
                                                    </li>  
                                                </ul>
                                            </li>

                                            

                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="javascript:void(0)"
                                                   data-bs-toggle="dropdown">Products and Services</a>

                                                <ul class="dropdown-menu">                                                
                                                    <li class="sub-dropdown-hover">
                                                        <a href="javascript:void(0)" class="dropdown-item">Diabsolve</a>
                                                        <ul class="sub-menu">
                                                            <li>
                                                                <a href="product-left-thumbnail.html">Description</a>
                                                            </li>

                                                            <li>
                                                                <a href="product-right-thumbnail.html">Ingredients</a>
                                                            </li>
                                                            <li>
                                                                <a href="product-bottom-thumbnail.html">Benefits</a>
                                                            </li>
                                                            <li>
                                                                <a href="product-bottom-thumbnail.html">Instructions to use</a>
                                                            </li>
                                                            <li>
                                                                <a href="product-bottom-thumbnail.html">Unit price</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li class="sub-dropdown-hover">
                                                        <a href="javascript:void(0)" class="dropdown-item">For skin care</a>
                                                        <ul class="sub-menu">
                                                            <li>
                                                                <a href="product-left-thumbnail.html">Description</a>
                                                            </li>

                                                            <li>
                                                                <a href="product-right-thumbnail.html">Ingredients</a>
                                                            </li>
                                                            <li>
                                                                <a href="product-bottom-thumbnail.html">Benefits</a>
                                                            </li>
                                                            <li>
                                                                <a href="product-bottom-thumbnail.html">Instructions to use</a>
                                                            </li>
                                                            <li>
                                                                <a href="product-bottom-thumbnail.html">Unit price</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li class="sub-dropdown-hover">
                                                        <a href="javascript:void(0)" class="dropdown-item">Treatment Packages</a>
                                                        <ul class="sub-menu">
                                                            <li>
                                                                <a href="product-left-thumbnail.html">Description</a>
                                                            </li>

                                                            <li>
                                                                <a href="product-right-thumbnail.html">Ingredients</a>
                                                            </li>
                                                            <li>
                                                                <a href="product-bottom-thumbnail.html">Benefits</a>
                                                            </li>
                                                            <li>
                                                                <a href="product-bottom-thumbnail.html">Instructions to use</a>
                                                            </li>
                                                            <li>
                                                                <a href="product-bottom-thumbnail.html">Unit price</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>

                                            <li class="nav-item">
                                                <a class="nav-link ps-xl-2 ps-0" href="<?php echo get_permalink(12); ?>">Facts</a>                                            
                                            </li>
                                            
                                            <li class="nav-item">
                                                <a class="nav-link ps-xl-2 ps-0" href="#">Careers</a>                                            
                                            </li>


                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="right-nav">
                                <div class="nav-number">
                                    <a href="tel:+94772008728"><img src="<?php bloginfo('template_directory'); ?>/assets/images/icon/music.png" class="img-fluid blur-up lazyload" alt="">
                                    <span>(+94) 77 200 8728</span></a>
                                </div>
                                <a href="www.facebook.com" class="btn theme-bg-color ms-3 fire-button"
                                   data-bs-toggle="modal" data-bs-target="#deal-box">
                                    
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                                
                                <a href="www.instagram.com" class="btn theme-bg-color ms-3 fire-button"
                                   data-bs-toggle="modal" data-bs-target="#deal-box">
                                    
                                    <i class="fab fa-instagram"></i>
                                </a>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Header End -->

        <!-- mobile fix menu start -->
        <div class="mobile-menu d-md-none d-block mobile-cart">
            <ul>
                <li class="active">
                    <a href="index.html">
                        <i class="iconly-Home icli"></i>
                        <span>Home</span>
                    </a>
                </li>

                <li class="mobile-category">
                    <a href="javascript:void(0)">
                        <i class="iconly-Category icli js-link"></i>
                        <span>Category</span>
                    </a>
                </li>

                <li>
                    <a href="search.html" class="search-box">
                        <i class="iconly-Search icli"></i>
                        <span>Search</span>
                    </a>
                </li>

                <li>
                    <a href="wishlist.html" class="notifi-wishlist">
                        <i class="iconly-Heart icli"></i>
                        <span>My Wish</span>
                    </a>
                </li>

                <li>
                    <a href="cart.html">
                        <i class="iconly-Bag-2 icli fly-cate"></i>
                        <span>Cart</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- mobile fix menu end -->

        <?php wp_head(); ?>