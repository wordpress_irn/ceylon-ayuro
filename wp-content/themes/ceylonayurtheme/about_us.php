<?php
/**
 * Template Name:  About Us
 * Description: About Us
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage ceylonAyuro
 * @since ceylonAyuro 1.0
 */
get_header();
//$page = get_post($page->ID);

//$sliders = get_field('sliders', $page->ID); 
?>

<!-- Breadcrumb Section Start -->
<section class="breadscrumb-section pt-0">
    <div class="container-fluid-lg">
        <div class="row">
            <div class="col-12">
                <div class="breadscrumb-contain">
                    <h2>About Us</h2>
                    <nav>
                        <ol class="breadcrumb mb-0">
                            <li class="breadcrumb-item">
                                <a href="index.html">
                                    <i class="fa-solid fa-house"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">About Us</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Section End -->

<!-- Fresh Vegetable Section Start -->
<section class="fresh-vegetable-section section-lg-space">
    <div class="container-fluid-lg">
        <div class="row gx-xl-5 gy-xl-0 g-3 ratio_148_1">
            <div class="col-xl-6 col-12">
                <div class="row g-sm-4 g-2">
                    <div class="col-6">
                        <div class="fresh-image-2">
                            <div>
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/inner-page/about-us/1.jpg"
                                     class="bg-img blur-up lazyload" alt="">
                            </div>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="fresh-image">
                            <div>
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/inner-page/about-us/2.jpg"
                                     class="bg-img blur-up lazyload" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-6 col-12">
                <div class="fresh-contain p-center-left">
                    <div>
                        <div class="review-title">
                            <h4>About Us</h4>
                            <h2>We are manufactured using 100% natural in-grown ingredients</h2>
                        </div>

                        <div class="delivery-list">
                            <p class="text-content">Ceylon Ayuro (Pvt) Ltd is a Sri Lankan pharmaceutical company which focuses on the research and 
                                development of pharmaceutical products and medicines with a special emphasis on health and wellness. 
                                Ceylon Ayuro products are manufactured using 100% natural in-grown ingredients. The herbs induced in our natural 
                                ingredients are being used for generations in the system of indigenous medicine to promote health and wellness. 
                                Ceylon Ayoro is a company that has brought together age-old herbal remedy & modern technology to produce a range 
                                of herbal products using natural active ingredients.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Fresh Vegetable Section End -->

<!-- Client Section Start -->
<section class="client-section section-lg-space">
    <div class="container-fluid-lg">
        <div class="row">
            <div class="col-12">
                <div class="about-us-title text-center">
                    <h4>What We Do</h4>
                    <h2 class="center">VISION | MISSION | VALUES</h2>
                </div>

                <div class="slider-3_1 product-wrapper">
                    <div>
                        <div class="clint-contain">
                            <div class="client-icon">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/svg/3/vision.png" class="blur-up lazyload" alt="">
                            </div>                                
                            <h4>Vision</h4>
                            <p>To be your most trusted health care partner while reinventing the values of Ayurveda 
                                through the intervention of modern technology; while preserving its natural goodness.</p>
                        </div>
                    </div>

                    <div>
                        <div class="clint-contain">
                            <div class="client-icon">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/svg/3/mission.png" class="blur-up lazyload" alt="">
                            </div>                                
                            <h4>Mission</h4>
                            <p>To promote the miraculous healing power of Ayurveda and the value of herbals of Ayurvedic medicine
                                locally as well as internationally. Improving people’s lives with our health care products in order to make the 
                                world a healthier place To provide patient-centred health care with excellence in quality and service raising 
                                the standards of Ayurvedic medicine.</p>
                        </div>
                    </div>

                    <div>
                        <div class="clint-contain">
                            <div class="client-icon">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/svg/3/values.png" class="blur-up lazyload" alt="">
                            </div>                                
                            <h4>Values</h4>
                            <p>Compassion</p>
                            <p>Integrity</p>
                            <p>Honesty</p>
                            <p>Solidarity</p>
                            <p>Quality</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Client Section End -->

<!-- Offer Section Start -->
<section class="offer-section">
    <div class="container-fluid-lg">
        <div class="row">
            <div class="col-12">
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/ad_banner_1.jpg" class="blur-up lazyload" alt="">                    
            </div>
        </div>
    </div>
</section>
<!-- Offer Section End -->

<?php get_footer(); ?>
</body>

</html>