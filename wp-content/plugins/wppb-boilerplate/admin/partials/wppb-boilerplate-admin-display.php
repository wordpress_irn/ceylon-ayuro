<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://wppb.me
 * @since      1.0.0
 *
 * @package    Wppb_Boilerplate
 * @subpackage Wppb_Boilerplate/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
