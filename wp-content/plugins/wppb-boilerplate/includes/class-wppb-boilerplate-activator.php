<?php

/**
 * Fired during plugin activation
 *
 * @link       https://wppb.me
 * @since      1.0.0
 *
 * @package    Wppb_Boilerplate
 * @subpackage Wppb_Boilerplate/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wppb_Boilerplate
 * @subpackage Wppb_Boilerplate/includes
 * @author     Imran <imran.fernando@gmail.com>
 */
class Wppb_Boilerplate_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
