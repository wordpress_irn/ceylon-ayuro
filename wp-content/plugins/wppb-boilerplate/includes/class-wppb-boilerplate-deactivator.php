<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://wppb.me
 * @since      1.0.0
 *
 * @package    Wppb_Boilerplate
 * @subpackage Wppb_Boilerplate/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wppb_Boilerplate
 * @subpackage Wppb_Boilerplate/includes
 * @author     Imran <imran.fernando@gmail.com>
 */
class Wppb_Boilerplate_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
