<?php
/**
 * Plugin Name:     WP-CLI Demo Plugin
 * Plugin URI:      https://plugins.wp-cli.org/demo-plugin
 * Description:     This is a wp-cli demo plugin
 * Author:          wp-cli
 * Author URI:      https://wp-cli.org
 * Text Domain:     wpcli-demo-plugin
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Wpcli_Demo_Plugin
 */

// Your code starts here.
